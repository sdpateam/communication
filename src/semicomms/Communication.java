package semicomms;

import jssc.*;
import vision.RobotType;

import java.util.List;
import java.util.LinkedList;

/**
 * Created by s1351669 on 26/02/16.
 */
public class Communication implements CommunicationInterface {

    private String robot1PortName = null;
    private String robot2PortName = null;
    private Port robot1Port = new Port(this, RobotType.FRIEND_1);
    private Port robot2Port = new Port(this, RobotType.FRIEND_2);

    private LinkedList<CommunicationListener> listeners;

    private static Communication coms = null;

    public static Communication getCommunication(){
        return coms;
    }

    public static Communication getCommunication(List<RobotType> robots){
        if(coms != null) return coms;
        boolean allConnected;
        try {
            do {
                if(coms != null) coms.closePorts();
                Thread.sleep(200);
                coms = new Communication();
                Thread.sleep(200);

                allConnected = true;

                for(RobotType type : robots){
                    System.out.println("[Comms] Trying to connect to: " + type.name());
                    allConnected = allConnected && coms.isConnected(type);
                }

            } while(!allConnected);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return coms;
    }

    private Communication() {
        this.listeners = new LinkedList<CommunicationListener>();
        String[] portNames = SerialPortList.getPortNames();
        SerialPort port;
        String response;
        for(String s : portNames){
            port = new SerialPort(s);
            try {
                port.openPort();
                port.purgePort(SerialPort.PURGE_RXCLEAR);
                port.readBytes(port.getInputBufferBytesCount());
                port.writeBytes("ping\n".getBytes());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("What the shit just happened, the Thread failed to sleep.. :/");
                }
                response = port.readString();
                if(response != null){
                    if(response.contains("pong")){
                        this.robot1Port.setSerialPort(port);
                        this.robot1PortName = s;
                        System.out.println("Found Robot 1");
                    } else if(response.contains("pang")){
                        this.robot2Port.setSerialPort(port);
                        this.robot2PortName = s;
                        System.out.println("Found Robot 2");
                    } else {
                        port.closePort();
                    }
                } else {
                    port.closePort();
                }
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    public void closePorts(){
        try {
            this.stop(RobotType.FRIEND_1);
            this.robot1Port.close();
        } catch (Exception e) {
            System.out.println("Robot 1 port already closed.");
        }
        try {
            this.stop(RobotType.FRIEND_2);
            this.robot2Port.close();
        } catch (SerialPortException e) {
            System.out.println("Robot 2 port already closed.");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Communication c = new Communication();
        c.closePorts();
        System.exit(0);
    }

    public boolean isConnected(RobotType type){
        if(type == RobotType.FRIEND_1) return this.robot1PortName != null;
        return this.robot2PortName != null;
    }


    public void addCommunicationListener(CommunicationListener listener){
        this.listeners.add(listener);
    }

    public void portEvent(RobotType type, String string) {
        for(CommunicationListener listener : this.listeners){
            listener.receivedStringHandler(type, string);
        }
    }


    private void commandSender(RobotType robot, String robot1Command, String robot2Command, int[] args){
        Port port = robot == RobotType.FRIEND_1 ? this.robot1Port : this.robot2Port;
        StringBuilder sb = new StringBuilder();
        sb.append(robot == RobotType.FRIEND_1 ? robot1Command : robot2Command);
        if(args != null){
            for(int i : args){
                sb.append(' ');
                sb.append(i);
            }
        }
        port.sendCommand(sb.toString());
    }


    @Override
    public void ping(RobotType robot) {
        this.commandSender(robot, "ping", "ping", null);
    }

    @Override
    public void move(RobotType robot, int ... args) {
        this.commandSender(robot, "G", "MOV", args);
    }

    @Override
    public void rotate(RobotType robot, int ... args) {
        this.commandSender(robot, "rotate", "ROT", args);
    }

    @Override
    public void kick(RobotType robot, int ... args) {
        if (robot == RobotType.FRIEND_1) {
            this.commandSender(robot, "shuntkick", "", args);
        } else {
            this.commandSender(robot, "", "KICK", args);
        }
    }

    public void rawSend(RobotType robot, String command){
        this.commandSender(robot, command, command, null);
    }

    @Override
    public void catchBall(RobotType robot, int ... args) {
        if (robot == RobotType.FRIEND_1) {
            if (args.length > 0) {
                this.commandSender(robot, "grab", "", args);
            } else {
                int[] commands = {1};
                this.commandSender(robot, "grab", "", commands);
            }
        } else {
            this.commandSender(robot, "", "CATCH", args);
        }
    }

    @Override
    public void stop(RobotType robot) {
        this.commandSender(robot, "S", "F", null);
    }

    @Override
    public void led(RobotType robot) {
        this.commandSender(robot, "L", "L", null);
    }

    public void moveForever(RobotType robot){
        int[] commands1 = {0, 1, 0, 255};
        if(robot == RobotType.FRIEND_1){
            this.commandSender(robot, "G", "", commands1);
        } else {
            this.commandSender(robot, "", "MOV", null);
        }
    }

    public void rotateForever(RobotType robot, int direction){
        int[] commands1 = {0, 0, direction, 150};
        int[] commands2 = {direction, 100};
        if(robot == RobotType.FRIEND_1){
            this.commandSender(robot, "G", "", commands1);
        } else {
            this.commandSender(robot, "", "ROT", commands2);
        }
    }
}
