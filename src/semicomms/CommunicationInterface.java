package semicomms;

//import communication.RobotType;

import vision.RobotType;

/**
 * Created by s1351669 on 16/02/16.
 */


public interface CommunicationInterface {

    public void ping(RobotType robot);

    public void move(RobotType robot, int ... args);

    public void rotate(RobotType robot, int ... args);

    public void kick(RobotType robot, int ... args);

    public void catchBall(RobotType robot, int ... args);

    public void stop(RobotType robot);

    public void led(RobotType robot);


}