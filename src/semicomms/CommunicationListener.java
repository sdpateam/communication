package semicomms;

import vision.RobotType;

/**
 * Created by s1351669 on 26/02/16.
 */
public interface CommunicationListener {
    public void receivedStringHandler(RobotType robotType, String string);
}
