package semicomms;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import vision.RobotType;

import java.util.ArrayList;

/**
 * Created by s1351669 on 26/02/16.
 */
public class Port implements SerialPortEventListener {

    private SerialPort port = null;
    private String lastCommand;
    private Communication creator;
    private RobotType robotType;
    private ArrayList<String> commandLog;
    private StringBuilder builder;

    public Port(Communication creator, RobotType robotType){
        this.robotType = robotType;
        this.creator = creator;
        this.commandLog = new ArrayList<String>();
        this.builder = new StringBuilder();
    }

    public void close() throws SerialPortException {
        if(this.port == null) return;
        this.port.closePort();
    }

    public void setSerialPort(SerialPort port){
        if(port == null) return;
        this.port = port;
        try {
            port.addEventListener(this);
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    public void sendCommand(String command){
        if(this.port == null){
            return;
        }
        this.lastCommand = command;
        System.out.println(this.robotType.toString() + " - COMMAND: '" + command + "'");
        try {
            this.port.writeBytes((command + "\n").getBytes());
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    private void nextString(String s){
        System.out.println(this.robotType.toString() + " says: '" + s + "'");
        this.creator.portEvent(this.robotType, s);
    }

    private void nextPacket(String s){
        this.nextString(s);
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        if(event.isRXCHAR() && event.getEventValue() > 0){
            try {
                byte buffer[] = this.port.readBytes();
                for (byte b: buffer) {
                    if (b == '\r' || b == '\n') {
                        if(this.builder.length() > 0){
                            String toProcess = this.builder.toString();
                            nextString(toProcess);
                            this.builder.setLength(0);
                        }
                    }
                    else {
                        this.builder.append((char)b);
                    }
                }
            }
            catch (SerialPortException ex) {
                System.out.println(ex);
                System.out.println("serialEvent");
            }
        }
    }
}
