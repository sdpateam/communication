%
% Communication technical specification
%

\section{Communication}

There are two classes that make the communication between the computer and the RF stick possible, \verb|Port| and \verb|Communication|.

The \verb|Port| class handles configuring and closing the serial port. Various interfaces for communicating over the serial port are implemented, providing varied levels of abstraction from sending raw data to command-set agnostic commands. The communications module uses the \verb|JSSC| library to talk to the Serial port.

\verb|Communication| provides an interface that abstracts the command set specifics away from the calling program (e.g. the Strategy module) allowing the command set to evolve over time without breaking Strategy. Given the unified approach taken by the team, to control both robots with the same strategy system, this also allows the same interface to be used for both bots, with any dissimilarities between their command sets and capabilities to be completely opaque to strategy.

Higher level commands can also be developed, effectively macros of multiple firmware commands that achieve a useful outcome; Kicking, grabbing or rotating may require multiple commands, sometimes with an initiating command and a terminating command. Providing a simplified method of controlling the robot to Strategy reduces the size of it's search space.

Responses to commands sent are handled with a \verb|CommunicationsListener| asynchronously; This is of course important we do not want to stall strategy, and relatively speaking responses take a very long time to propagate back to Comms. If a \verb|NACK| is received, or after a time-out threshold the robot appears to have missed the command it is resent quietly, without disrupting Strategy's operation.


\subsection{Command Set}

All commands should be sent with a trailing newline, \verb|\n|

\begin{description}

\item[Ping]
  This is the simplest command the firmware responds to and should immediately receive a \verb|pong| back.

  send: \verb|ping|
  
  gets: \verb|pong|
  

\item[Toggle LED]
  Toggles the LED on/off. This will get overridden pretty quickly by the heartbeat blinking, but you can verify your connection this way too.

  send: \verb|L\n|
  
  gets: nothing

\item[Go] 
  Give the robot a $(x,y,\omega)$ vector and a power in the range 0 - 255, which it'll use to calculate and execute the holonomic motion in that direction. $x$ is the lateral (right +ve) direction, $y$ is the forward and $\omega$ is the rotational speed (anti-clockwise +ve). Note that the bot will always execute the motion at the maximum speed possible, so the \verb|rotate| command should be used for precise rotations. The bounds on the variables are undefined, as their relative magnitudes are used though the recommended range is $-1.0$ to $1.0$. \verb|G 0 0 0 0| or \verb|G| will stop the robot. If the fourth argument is omitted then it will default to 255 (full power).

  send: \verb|G %f1 %f2 %f3 %d4|

  gets: \verb|A|

  debug: \verb|Going|
  
\item[Move] 
  Tells the robot to apply power to the motors in the order 1 $>$ 2 $>$ 3. Valid range is -255 to 255 inclusive. \verb|M 0 0 0| or \verb|M| will stop the robot. This provides lower level / more direct control of the motors, and in general `Go' is preferable.

  send: \verb|M %d1 %d2 %d3|
  
  gets: \verb|A|

  debug: \verb|Moving|

  
\item[Force Stop]
  Force a stop. The robot will remove power from the motors immediately, without trying to do any clever `quick stop` by braking the motors. Generally a singular \verb|M| or \verb|G| will work better.
  
  send: \verb|S|

  gets: \verb|A|

  debug: \verb|Force stopping|

  
\item[Motor Speeds]
  Will print the instantaneous speeds of all three drive motors in bogounits (encoder stops per second) in the form:

  send: \verb|speeds|
  
  gets: \verb|0: n.nn 1: n.nn 2: n.nn|


\item[Grabbing]
  Opens or closes the grabber, depending on the argument given

  send: \verb|grab <0/1>|

  gets: \verb|A|

  debug: \verb|grabbing <0/1>\n done|


\item[Rotating]
  Rotates at \texttt{d1} power (-255 to 255 inclusive) until the encoders have gone through \texttt{d2} stops (0+) on average. This can be used to get fairly precise rotation but stops isn't degrees (though it's related) and depends somewhat on battery voltage.

  send: \verb|rotate %d1 %d2|

  gets: \verb|A|

  debug: \verb|rotating at %d1 to %d2 stops|


\item[Help]
  Prints the commands that the robot is listening for; this is always correct as it is generated on-the-fly from the Command Set parsing class. It won't print how many arguments it expects, but is useful for checking spelling or troubleshooting a \texttt{N} (NACK).

  The help command can also be used to verify that the command set is the one you're expecting.

  send: \verb|help|

  get: \verb|Valid input commands: (some have arguments)|\\
  (Each command will be printed on a new line, excluding the number of arguments)


\item[NACK]
  If the command is not recognised the bot will reply with a NACK:

  \verb|N - <unrecognised_command>|

\end{description}

